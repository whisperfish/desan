use desan_derive::SanitizeDebug;
use uuid::Uuid;

#[derive(SanitizeDebug)]
struct Foobar {
    #[desan(retain(..4, '*'))]
    username: String,
    #[desan(replace("****"))]
    password: Option<String>,
    #[desan(retain_first_group('a'))]
    uuid: Option<Uuid>,
}

#[test]
fn some() {
    let f = Foobar {
        username: "rubdos".into(),
        password: Some("very secure".into()),
        uuid: Some(Uuid::parse_str("cc34d930-3907-4d98-b3bd-a8a5e08d6a60").unwrap()),
    };

    assert_eq!(
        format!("{:?}", f),
        r#"Foobar { username: "rubd**", password: Some("****"), uuid: Some(cc34d930-aaaa-aaaa-aaaa-aaaaaaaaaaaa) }"#
    );
}

#[test]
fn none() {
    let f = Foobar {
        username: "rubdos".into(),
        password: None,
        uuid: None,
    };

    assert_eq!(
        format!("{:?}", f),
        r#"Foobar { username: "rubd**", password: None, uuid: None }"#
    );
}
