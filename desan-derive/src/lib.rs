use proc_macro::TokenStream;
use quote::quote;
use syn::parse_macro_input;

fn derive_sanitize_debug_impl(input: syn::DeriveInput) -> proc_macro2::TokenStream {
    let ident = &input.ident;

    match &input.data {
        syn::Data::Struct(s) => derive_sanitize_debug_struct(&input, s),
        // syn::Data::Enum(e) => derive_sanitize_debug_enum(&input, e)
        _ => unimplemented!("Cannot derive SanitizeDebug on {}", ident),
    }
}

enum FieldType {
    Named,
    Unnamed(usize),
}

fn sanitize_field(field: &syn::Field, field_type: FieldType) -> proc_macro2::TokenStream {
    // extract attribute
    let attribute = field.attrs.iter().find(|attr| {
        if let Some(f) = &attr.path.segments.first() {
            f.ident == "desan"
        } else {
            false
        }
    });

    if let Some(attr) = attribute {
        let attr: syn::Expr = attr.parse_args().expect("attribute syntax");
        let ty = &field.ty;
        match extract_type_from_option(ty) {
            Some(inner_ty) => {
                let helper = match attr {
                    syn::Expr::Call(call) => {
                        let func = &call.func;
                        let args = call.args.iter();
                        quote! {
                            #func(inner, #(#args,)*)
                        }
                    }
                    _ => panic!("unparsable attribute"),
                };

                match field_type {
                    FieldType::Named => {
                        let name = field.ident.as_ref().unwrap();
                        quote! {
                            match &self.#name {
                                Some(inner) => s.field(stringify!(#name), &Some(&<#inner_ty as desan::Sanitizable<'_, _>>::Sanitized::#helper)),
                                None => s.field(stringify!(#name), &Option::None::<&#inner_ty>),
                            }
                        }
                    }
                    FieldType::Unnamed(idx) => {
                        quote! {
                            match &self.#idx {
                                Some(inner_ty) => s.field(&<#inner_ty as desan::Sanitizable<'_, _>>::Sanitized::#helper),
                                None => s.field(Option::None::<&#inner_ty>),
                            }
                        }
                    }
                }
            }
            None => match attr {
                syn::Expr::Call(call) => {
                    let func = &call.func;
                    let args = call.args.iter();
                    match field_type {
                        FieldType::Named => {
                            let name = field.ident.as_ref().unwrap();
                            quote! {
                                s.field(stringify!(#name), &<#ty as desan::Sanitizable<'_, _>>::Sanitized::#func(&self.#name, #(#args,)*))
                            }
                        }
                        FieldType::Unnamed(idx) => {
                            quote! {
                                s.field(&<#ty as desan::Sanitizable<'_, _>>::Sanitized::#func(&self.#idx, #(#args,)*))
                            }
                        }
                    }
                }
                _ => panic!("unparsable attribute"),
            },
        }
    } else {
        match field_type {
            FieldType::Named => {
                let name = field.ident.as_ref().unwrap();
                quote! { &self.#name }
            }
            FieldType::Unnamed(idx) => {
                quote! { &self.#idx }
            }
        }
    }
}

fn extract_type_from_option(ty: &syn::Type) -> Option<&syn::Type> {
    use syn::{GenericArgument, Path, PathArguments, PathSegment};

    fn extract_type_path(ty: &syn::Type) -> Option<&Path> {
        match *ty {
            syn::Type::Path(ref typepath) if typepath.qself.is_none() => Some(&typepath.path),
            _ => None,
        }
    }

    // TODO maybe optimization, reverse the order of segments
    fn extract_option_segment(path: &Path) -> Option<&PathSegment> {
        let idents_of_path = path
            .segments
            .iter()
            .into_iter()
            .fold(String::new(), |mut acc, v| {
                acc.push_str(&v.ident.to_string());
                acc.push('|');
                acc
            });
        ["Option|", "std|option|Option|", "core|option|Option|"]
            .iter()
            .find(|s| &idents_of_path == *s)
            .and_then(|_| path.segments.last())
    }

    extract_type_path(ty)
        .and_then(|path| extract_option_segment(path))
        .and_then(|path_seg| {
            let type_params = &path_seg.arguments;
            // It should have only on angle-bracketed param ("<String>"):
            match *type_params {
                PathArguments::AngleBracketed(ref params) => params.args.first(),
                _ => None,
            }
        })
        .and_then(|generic_arg| match *generic_arg {
            GenericArgument::Type(ref ty) => Some(ty),
            _ => None,
        })
}

fn derive_sanitize_debug_struct(
    input: &syn::DeriveInput,
    stru: &syn::DataStruct,
) -> proc_macro2::TokenStream {
    let ident = &input.ident;

    let (s, fields): (_, Vec<proc_macro2::TokenStream>) = match &stru.fields {
        syn::Fields::Unit => (quote!(fmt.debug_tuple(stringify!(#ident))), vec![]),
        syn::Fields::Unnamed(unnamed_fields) => {
            let fields = unnamed_fields
                .unnamed
                .iter()
                .enumerate()
                .map(|(idx, f)| sanitize_field(f, FieldType::Unnamed(idx)))
                .collect();
            (quote!(fmt.debug_tuple(stringify!(#ident))), fields)
        }
        syn::Fields::Named(named_fields) => {
            let fields = named_fields
                .named
                .iter()
                .map(|f| sanitize_field(f, FieldType::Named))
                .collect();
            (quote!(fmt.debug_struct(stringify!(#ident))), fields)
        }
    };

    quote! {
        impl desan::SanitizeDebug for #ident {
            fn fmt_sanitized(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                let mut s = #s;
                #(#fields;)*
                s.finish()
            }
        }

        impl std::fmt::Debug for #ident {
            fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                use desan::SanitizeDebug;
                self.fmt_sanitized(fmt)
            }
        }
    }
}

#[proc_macro_derive(SanitizeDebug, attributes(desan))]
pub fn derive_sanitize_debug(item: TokenStream) -> TokenStream {
    derive_sanitize_debug_impl(parse_macro_input!(item)).into()
}
