# Desan

`#[derive(Debug)]`, with sanitized fields.
Very useful in combination with debug logs.

```rust
#[derive(SanitizeDebug)]
struct Foobar {
    #[desan::retain(..4, '*')]
    username: String,
    #[desan::replace("****")]
    password: String,
}

#[test]
fn test_foobar() {
    let f = Foobar {
        username: "rubdos".into(),
        password: "very secure".into(),
    };
    assert_eq!(format!("{:?}", f), r#"Foobar {
        username: "rubd**",
        password: "****",
    }"#);
}
```

# License

Licensed under either of Apache License, Version 2.0 or MIT license at your option.

Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in Desan by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any additional terms or conditions.
