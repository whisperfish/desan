use std::fmt::{Debug, Formatter};

use uuid::Uuid;

use crate::{Sanitizable, SanitizeDebug};

pub struct SanitizedUuid<'a>(&'a Uuid, Mode);

#[derive(Clone, Debug)]
pub enum Mode {
    ReplaceAll { replacement: char },
    ShowFirstGroup { replacement: char },
}

impl<'a> SanitizedUuid<'a> {
    pub fn retain_first_group(uuid: &'a Uuid, replacement: char) -> Self {
        Self(uuid, Mode::ShowFirstGroup { replacement })
    }

    pub fn replace_all(uuid: &'a Uuid, replacement: char) -> Self {
        Self(uuid, Mode::ReplaceAll { replacement })
    }
}

impl<'a> Debug for SanitizedUuid<'a> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.fmt_sanitized(f)
    }
}

impl<'a> SanitizeDebug for SanitizedUuid<'a> {
    fn fmt_sanitized(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self.1 {
            Mode::ReplaceAll { replacement } => {
                // 550e8400-e29b-41d4-a716-446655440000
                //    8    -  4 -  4 -  4 -     12
                let s = replacement.to_string();
                let parts = [8, 4, 4, 4, 12];
                let mut parts = parts.iter().map(|n| s.repeat(*n)).peekable();
                while let Some(foo) = parts.next() {
                    if parts.peek().is_some() {
                        write!(f, "{}-", foo)?;
                    } else {
                        write!(f, "{}", foo)?;
                    }
                }
            }
            Mode::ShowFirstGroup { replacement } => {
                let clear = format!("{}", self.0);
                let first = clear.split('-').next().unwrap();
                write!(f, "{}-", first)?;

                // 550e8400-e29b-41d4-a716-446655440000
                //    X    -  4 -  4 -  4 -     12
                let s = replacement.to_string();
                let parts = [4, 4, 4, 12];
                let mut parts = parts.iter().map(|n| s.repeat(*n)).peekable();
                while let Some(foo) = parts.next() {
                    if parts.peek().is_some() {
                        write!(f, "{}-", foo)?;
                    } else {
                        write!(f, "{}", foo)?;
                    }
                }
            }
        }
        Ok(())
    }
}

impl<'s> Sanitizable<'s, Mode> for Uuid {
    type Sanitized = SanitizedUuid<'s>;

    fn sanitize(&'s self, mode: Mode) -> Self::Sanitized {
        SanitizedUuid(self, mode)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_uuid_show_first() {
        let uuid = uuid::Uuid::new_v4();
        let f_orig = format!("{:?}", uuid);
        let uuid = SanitizedUuid(&uuid, Mode::ShowFirstGroup { replacement: 'x' });
        let f = format!("{:?}", uuid);
        assert_eq!(f.len(), 32 + 4);
        assert_eq!(&f[8..], "-xxxx-xxxx-xxxx-xxxxxxxxxxxx");
        assert_eq!(&f[..8], &f_orig[..8]);
    }

    #[test]
    fn test_uuid_replace() {
        let uuid = uuid::Uuid::new_v4();
        let uuid = SanitizedUuid(&uuid, Mode::ReplaceAll { replacement: 'x' });
        let f = format!("{:?}", uuid);
        assert_eq!(f.len(), 32 + 4);
        assert_eq!(f, "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx");
    }
}
