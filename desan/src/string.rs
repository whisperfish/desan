use std::fmt::{Debug, Formatter};
use std::ops::{Bound, RangeBounds};

use crate::Sanitizable;

use super::SanitizeDebug;

pub struct SanitizedString<'a>(&'a str, Mode);

#[derive(Clone, Debug)]
pub enum Mode {
    Retain {
        range: (Bound<usize>, Bound<usize>),
        replacement: char,
    },
    Replace {
        replacement: &'static str,
    },
}

impl Mode {
    fn apply_to<'out, 's: 'out, 'a: 'out>(
        &'s self,
        target: &'a str,
    ) -> std::borrow::Cow<'out, str> {
        match self {
            Mode::Retain { range, replacement } => {
                if range.contains(&(target.len() - 1)) && range.contains(&0) {
                    // no-op
                    target.into()
                } else {
                    (target.chars().enumerate())
                        .map(|(i, c)| if range.contains(&i) { c } else { *replacement })
                        .collect()
                }
            }
            Mode::Replace { replacement } => (*replacement).into(),
        }
    }
}

impl<'a> SanitizedString<'a> {
    pub fn retain(s: &'a str, range: impl RangeBounds<usize>, replacement: char) -> Self {
        Self(
            s,
            Mode::Retain {
                range: (range.start_bound().cloned(), range.end_bound().cloned()),
                replacement,
            },
        )
    }

    pub fn replace(s: &'a str, replacement: &'static str) -> Self {
        Self(s, Mode::Replace { replacement })
    }
}

impl<'a> Debug for SanitizedString<'a> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.fmt_sanitized(f)
    }
}

impl<'a> SanitizeDebug for SanitizedString<'a> {
    fn fmt_sanitized(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.1.apply_to(self.0).as_ref())
    }
}

impl<'s> Sanitizable<'s, Mode> for str {
    type Sanitized = SanitizedString<'s>;

    fn sanitize(&'s self, mode: Mode) -> Self::Sanitized {
        SanitizedString(self, mode)
    }
}

impl<'s> Sanitizable<'s, Mode> for String {
    type Sanitized = SanitizedString<'s>;

    fn sanitize(&'s self, mode: Mode) -> Self::Sanitized {
        (self as &str).sanitize(mode)
    }
}
