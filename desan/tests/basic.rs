use std::fmt::{Debug, Error, Formatter};

use desan::{SanitizeDebug, SanitizedString};

struct Foobar {
    username: String,
    password: String,
}

impl SanitizeDebug for Foobar {
    fn fmt_sanitized(&self, fmt: &mut Formatter<'_>) -> Result<(), Error> {
        fmt.debug_struct("Foobar")
            .field(
                "username",
                &SanitizedString::retain(&self.username, ..4, '*'),
            )
            .field(
                "password",
                &SanitizedString::replace(&self.password, "****"),
            )
            .finish()
    }
}

impl Debug for Foobar {
    fn fmt(&self, fmt: &mut Formatter<'_>) -> Result<(), Error> {
        self.fmt_sanitized(fmt)
    }
}

#[test]
fn test_foobar() {
    let f = Foobar {
        username: "rubdos".into(),
        password: "very secure".into(),
    };

    assert_eq!(
        format!("{:?}", f),
        r#"Foobar { username: "rubd**", password: "****" }"#
    );
}
